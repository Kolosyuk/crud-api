import * as http from 'node:http';
import * as dotenv from 'dotenv';
import RequestHandler from './models/request-handler';
import Router from './models/router'

dotenv.config();

const server = http.createServer();

server.on('request', (request, response) => {
  const rh = new RequestHandler(request, response);
  const router = new Router();
  router.pathChecker(request.url, rh.handleBadRequest, rh.handleRequest); 
});

console.log(`Server listening on port ${process.env.PORT}`);


server.listen(process.env.PORT);