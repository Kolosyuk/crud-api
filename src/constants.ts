enum Methods {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
};

enum Statuses  {
  OK = 200,
  CREATED = 201,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500,
};

const Messages = {
  POST_PUT_FIELDS: `request body does not contain required fields - username(str), age(num), hobbies(str[])`,
  WRONG_FORMAT_DATA: `should be JSON`,
  USER_NOT_FOUND: `user not found, check ID and try again`,
  ID_PATTERN: 'wrong ID pattern',
  URL: 'check your URL',
  CREATED: `user created`,
  UPDATED: `user updated`,
  DELETED: `user deleted`,
  ERROR: `something goes wrong`,
}

export {
  Methods,
  Statuses,
  Messages
}