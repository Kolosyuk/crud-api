import { Messages, Methods, Statuses } from '../constants';
import { collectRequestData } from '../utils';
import { IncomingMessage, ServerResponse } from 'node:http';
import db from './db';
import User from './user';

interface IRequestHandler {
  request: IncomingMessage;
  path: string;
  method: Methods;
  status: Statuses;
  response: ServerResponse;
  handleRequest: () => void;
  handleBadRequest: ( status: Statuses, message: string) => void;
};

class RequestHandler implements IRequestHandler {
  request: IncomingMessage;
  path: string;
  method: Methods;
  status: Statuses; 
  response: ServerResponse;

  constructor(request : IncomingMessage, response: ServerResponse) {
    this.request = request;
    this.path = request.url;
    this.method = request.method as Methods;
    this.response = response;
  }

  handleRequest = () => {
    
    const replStr = /\//g;
    const slicedPath = this.path.replace(replStr, ' ').trim().split(' ');

    if(/GET/.test(this.method) && slicedPath.length === 2) {
      this.response.writeHead(Statuses.OK);
      this.response.end(JSON.stringify(db.getAllUsers()));
      return;
    };
    
    if(/GET/.test(this.method) && slicedPath.length === 3) {
      if (typeof db.getUser(slicedPath[2]) === 'string') {
        this.handleBadRequest(Statuses.NOT_FOUND, Messages.USER_NOT_FOUND)
        return;
      }
      this.response.writeHead(Statuses.OK);
      this.response.end(JSON.stringify(db.getUser(slicedPath[2])));
      return;
    };
    
    if(/POST/.test(this.method) && slicedPath.length === 2) {
      collectRequestData(this.request, this.handleBadRequest, ({ username, age, hobbies}) => {
        if ( username && age && hobbies ) {
          db.createUser(username, age, hobbies)
          this.response.writeHead(Statuses.CREATED);
          this.response.end(JSON.stringify({
            message: Messages.CREATED,
          }));
          return;
        } 
        this.handleBadRequest(Statuses.BAD_REQUEST, Messages.POST_PUT_FIELDS)
      })
      return;
    };

    if(/PUT/.test(this.method) ) {
      const id = slicedPath[2];
      
      collectRequestData(this.request,this.handleBadRequest, ({ username, age , hobbies }) => {
        if ( id && username && age && hobbies ) {
          if(typeof db.getUser(id) !== 'string') {
            db.updateUser({ id, username, age, hobbies } as User) ;
            this.response.writeHead(Statuses.CREATED);
            this.response.end(JSON.stringify({
              message: Messages.UPDATED,
            }));
            return;
          }
          this.handleBadRequest(Statuses.NOT_FOUND, Messages.USER_NOT_FOUND);
          return;
        } 
        this.handleBadRequest(Statuses.NO_CONTENT, Messages.POST_PUT_FIELDS)
      })
      return;
    };

    if(/DELETE/.test(this.method) ) {
      const id = slicedPath[2];
      if(typeof db.getUser(id) !== 'string') {
        db.deleteUser( id ) ;
        this.response.writeHead(Statuses.NO_CONTENT);
        this.response.end(JSON.stringify({
          message: Messages.DELETED,
        }));
        return;
      }
      this.handleBadRequest(Statuses.NOT_FOUND, Messages.USER_NOT_FOUND);
      return;
    };

    console.log('handle request, no option');
    
  };

  handleBadRequest = (status, message) => {
    switch (status) {
      case Statuses.NO_CONTENT:
        this.response.writeHead(Statuses.NO_CONTENT);
        this.response.end(message);
        break;

      case Statuses.BAD_REQUEST:
        this.response.writeHead(Statuses.BAD_REQUEST);
        this.response.end(message);
        break;
      case Statuses.NOT_FOUND:
        this.response.writeHead(Statuses.NOT_FOUND);
        this.response.end(message);
        break;

      default:
        break;
    }
  };
};

export default RequestHandler;