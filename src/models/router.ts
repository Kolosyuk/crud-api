import { validate } from 'uuid';
import { Messages, Statuses } from '../constants';

type RouterErrorCallBack = () => void;

type RouterCallBack = () => void;

interface IRouter {
  pathChecker (path : string, errCb : RouterErrorCallBack, cb : RouterCallBack) : void;
}

class Router implements IRouter {
  path: string;

  pathChecker(path, errorCb, cb) {
    const replStr = /\//g;
    const pathParths = path.replace(replStr, ' ').trim().split(' ');

    if (pathParths.length > 3) {
      errorCb(Statuses.NOT_FOUND, Messages.URL);
      return;
    }

    if (!/^api$/.test(pathParths[0].toLowerCase())) {
      errorCb(Statuses.NOT_FOUND, Messages.URL);
      return;
    };

    if (/^api$/.test(pathParths[0].toLowerCase()) && !pathParths[1]) {
      errorCb(Statuses.NOT_FOUND, Messages.URL);
      return;
    };

    if (pathParths[1] && !/^users$/.test(pathParths[1].toLowerCase())) {
      errorCb(Statuses.NOT_FOUND, Messages.URL);
      return;
    };

    if (pathParths[2] && !validate(pathParths[2].toLowerCase())) {
      errorCb(Statuses.BAD_REQUEST, Messages.ID_PATTERN);
      return;
    }

    cb();
  }
};

export default Router;