import { generateUniqueId } from '../utils';

interface IUser {
  id: string;
  username: string;
  age: number;
  hobbies: string[];
}

class User implements IUser {
  id: string;
  username: string;
  age: number;
  hobbies: string[];

  constructor(username : string, age : number, hobbies : Array<string>) {
    this.id = generateUniqueId();
    this.username = username;
    this.age = age;
    this.hobbies = hobbies;
  };
};

export default User;