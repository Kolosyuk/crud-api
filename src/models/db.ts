import User from './user'

let instance = null;

interface IDB {
  db: User[];
  getAllUsers: () => User[];
  getUser: (userId : string) => User | string;
  createUser: (username : string, age : number, hobbies : Array<string>) => void;
  updateUser: (user : User) => void;
  deleteUser: (userId : string) => void;
};

type Queue = {
  pid: number;
  route: string;
  // method: методы инстанса БД
}

class DB implements IDB {
  db: User[];
  _queue: Queue[];

  constructor () {

    this.db = [];
    if(!instance) {
      instance === this;
    }
    return instance;
  };

  getAllUsers() {
    return this.db;
  };

  getUser(userId : string) {
    const user = this.db.filter((user) => user.id === userId)[0]
    if(!user) {
      return 'User not found'
    }
    return user;
  };

  createUser(username : string, age : number, hobbies : Array<string>) {
    this.db.push( new User(username, age, hobbies))
  };

  updateUser(user : User) {
    this.db = this.db.reduce((acc : User[], item : User) => {
      if (item.id !== user.id){
        acc.push(item);
        return acc;
      };
      acc.push(user);
      return acc; 
    }, []);
  };

  deleteUser(userId : string) {
    this.db = this.db.filter((user) => user.id !== userId)
  };
};

const db = new DB();

export default db;