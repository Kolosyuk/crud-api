import { Messages, Statuses } from '../constants';

export function collectRequestData(request, errorCallback, callback) {
  let buff: Uint8Array[] = [];
  request.on('data', ( chunk : Uint8Array ) => {
      buff.push(chunk);
  });
  request.on('end', () => {
      const body = Buffer.concat(buff).toString();
      try {
        callback(JSON.parse(body));
      } catch (error) {
        console.log('recieved wrong data');
        errorCallback(Statuses.BAD_REQUEST, Messages.WRONG_FORMAT_DATA)
      }
  });
  request.on('error', () => {
    errorCallback(Statuses.INTERNAL_SERVER_ERROR, Messages.ERROR);
  })
};