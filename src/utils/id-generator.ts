import { v4 as uuid } from 'uuid';

export const generateUniqueId = () => {
  const id = uuid();
  return id;
}